# OpenML dataset: Aircraft-Pricing-Dataset

https://www.openml.org/d/43737

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

For a more comprehensive dataset with many more features check out the "Yacht/Motorboat Pricing Data (10,000+ listings)" dataset.
Link below:
https://www.kaggle.com/artemkorottchenko/large-boatyacht-pricing-dataset
Context
What are the most important features in determining the price of a new or used aircraft? Is it the aircraft type? Year? Manufacturer? Other characteristics? 
This is one of many questions regarding the used/new aircraft markets I hope to answer with this dataset. 
The dataset contains over 2000 aircraft that are for sale around the world. The data was scraped during July of 2020.
Content
The data was scraped from various websites using the Scrapy framework for Python. 
Scrapy script:
https://github.com/akorott/Aircraft-Scrapy-Script.git
Content scraped:

New/Used
Price  
Currency (USD, EUR, GBP)
Category
Year
Make
Model
Location
Serial number
Registration number
Total hours
Engine 1 hours 
Engine 2 hours 
Prop 1 hours
Prop 2 hours
Total Seats
Flight Rules
National Origin

Keep in mind that the data was scraped from 2 different sources. Some of the data (New/Used, Engine 1 hours, Engine 2 hours, Prop 1 hours, Prop 2 hours, Total Seats, Flight Rules) was only easily accessible on one source, thus is missing for part of the dataset. 
FAQ
Flight Rules: Visual Flight Rules (VFR) VS Instrument Flight Rules (IFR). In a nutshell, an aircraft equipped with IFR is one where a pilot can fully navigate an aircraft using instruments in the cockpit. Any aircraft flying over 18,000 feet, by law, has to be equipped with IFR equipment.
BTH - Beyond the Horizon - according to my research, BTH means that an aircraft is equipped with a radar, but doesn't fully meet IFR criteria.
VFR - (https://en.wikipedia.org/wiki/Visual_flight_rules)
IFR - (https://en.wikipedia.org/wiki/Instrument_flight_rules)
Some of the acronyms used within total hours, engine 1, engine 2, prop 1, prop 2 columns:
SMOH - Since major overhaul
SNEW - Since new
SPOH - Since prop overhaul
SFOH - Since factory overhaul (more reliable)
SOH - Since overhaul
STOH - Since top overhaul
SFRM - Since factory re-manufactured
Thank you for checking out this dataset and happy kaggling!

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43737) of an [OpenML dataset](https://www.openml.org/d/43737). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43737/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43737/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43737/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

